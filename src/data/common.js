import _ from 'lodash';
import { getColorForStatus } from './status';

const getAssignee = (assignee) => {
  return assignee && {
    name: assignee.displayName,
    avatarSrc: assignee.avatarUrls['48x48'],
  };
};

export const issueToTicketUpdate = (issue) => ({
  assignee: getAssignee(issue.fields.assignee),
  ticket: {
    id: issue.key,
    name: issue.fields.summary,
  },
  updatedStatus: {
    label: issue.fields.status.name,
    color: getColorForStatus({ id: issue.fields.status.id })
  },
});

export const getProgress = (issues, statusList, collectionName) => {
  const statusMap = _.keyBy(statusList, 'id');

  const issuesByStatus = _(issues)
    .groupBy('fields.status.id')
    .value();

  return _(issuesByStatus)
    .map((issues, id) => ({
      id,
      collectionName,
      count: issues.length,
      color: statusMap[id].color,
      name: statusMap[id].name,
      ticketsUpdated: _.map(issues, issueToTicketUpdate),
    }))
    .value();
};

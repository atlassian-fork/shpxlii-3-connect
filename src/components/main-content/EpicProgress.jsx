import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import BarGraph from '../common/BarGraph.jsx';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Counter = styled.div`
  font-size: 14px;
  color: #000000;
  margin-bottom: 3px;
`;

const NewIssueCounter = styled.span`
  font-size:14px;
  color: #36B37E;
  margin-left: 8px; 
`;

const GraphWrapper = styled.div`
  width: 100%;
  height: 24px;
  margin-top: 9px;
`;

class EpicProgress extends React.PureComponent {
    static propTypes = {
      issueCount: PropTypes.number.isRequired,
      newIssueCount: PropTypes.number.isRequired,
      progressData: BarGraph.propTypes.data,
    }

    render() {
      const { issueCount, newIssueCount, progressData } = this.props;

      return (
        <Container>
          <Counter>{issueCount} issues {newIssueCount > 0 ? (<NewIssueCounter>{newIssueCount} new issues</NewIssueCounter>) : null}</Counter>
          <GraphWrapper>
            <BarGraph data={progressData} cutoff={3}/>
          </GraphWrapper>
        </Container>
      );
    }
};

export default EpicProgress;

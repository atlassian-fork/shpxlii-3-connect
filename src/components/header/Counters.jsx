import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const CounterContainer = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const CounterItem = styled.div`
  font-size: 21px;
  color: #000000;
  margin-right: 10px;
`;

class Counters extends React.PureComponent {
    static propTypes = {
      epicCount: PropTypes.number.isRequired,
      issueCount: PropTypes.number.isRequired,
    }

    render() {
      const { epicCount, issueCount } = this.props;
      let content = null;
      if (epicCount > 0 || issueCount > 0) {
        content = [<CounterItem>{epicCount} epics</CounterItem>, <CounterItem>{issueCount} issues</CounterItem>];
      } else {
        content = <CounterItem>No epics or issues exist for this project.</CounterItem>;
      }

      return (
        <CounterContainer>
          {content}
        </CounterContainer>
      );
    }
}

export default Counters;

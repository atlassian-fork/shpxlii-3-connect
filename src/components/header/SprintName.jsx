import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Name = styled.div`
    font-size: 29px;
    font-weight: bold;
    color: #172B4D;
`;

class SprintName extends React.PureComponent {
    static propTypes = {
      sprintName: PropTypes.string.isRequired,
    }

    render() {
      return <Name>{this.props.sprintName}</Name>;
    }
}

export default SprintName;

import React from 'react';
import styled from 'styled-components';

import OverallProgress from './OverallProgress.jsx';
import SprintSummary from './SprintSummary.jsx';

const HeaderContainer = styled.div`
   display: flex;
   flex-direction: column;
   justify-content: space-between;
   margin: 32px 0;
`;

class SprintHeader extends React.PureComponent {
    static propTypes = {
      ...OverallProgress.propTypes,
      ...SprintSummary.propTypes,
    }

    render() {
      const { sprintName, sprintEndDate, teamMembers, epicCount, issueCount, legendItems, overallProgressData } = this.props;

      return (
        <HeaderContainer>
          <SprintSummary
            sprintName={sprintName}
            sprintEndDate={sprintEndDate}
            teamMembers={teamMembers}
          />
          <OverallProgress
            epicCount={epicCount}
            issueCount={issueCount}
            legendItems={legendItems}
            overallProgressData={overallProgressData}
          />
        </HeaderContainer>
      );
    }
}

export default SprintHeader;

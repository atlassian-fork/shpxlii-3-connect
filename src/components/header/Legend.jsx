import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LegendItem from './LegendItem.jsx';

const LegendContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

class Legend extends React.PureComponent {
    static propTypes = {
      legendItems: PropTypes.arrayOf(PropTypes.shape(LegendItem.propTypes)).isRequired,
    }

    render() {
      const { legendItems } = this.props;

      return (
        <LegendContainer>
          {
            legendItems.map((item, idx) =>
              <LegendItem
                key = {idx}
                color={item.color}
                label={item.label}
              />
            )
          }
        </LegendContainer>
      );
    }
}

export default Legend;

import React from 'react';
import styled from 'styled-components';

import Spinner from '@atlaskit/spinner';

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 64px;
`;

const Text = styled.div`
  margin-left: 16px;
  font-size: 22px;
  color: #344563;
`;

class Loading extends React.PureComponent {
  render() {
    return (
      <Wrapper>
        <Spinner size="large"/>
        <Text>Loading</Text>
      </Wrapper>
    );
  }
};

export default Loading;

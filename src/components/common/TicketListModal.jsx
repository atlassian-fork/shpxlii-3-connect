import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Modal from '@atlaskit/modal-dialog';

import TicketUpdate from '../common/TicketUpdate.jsx';

const UpdateWrapper = styled.div`
  margin-bottom: 3px;
`;

const ContentWrapper = styled.div`
  margin-bottom: 16px;
`;

class TicketListModal extends React.PureComponent {
  static propTypes = {
    title: PropTypes.string.isRequired,
    ticketsUpdated: PropTypes.arrayOf(PropTypes.shape(TicketUpdate.propTypes)).isRequired,
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
  };

  render() {
    const { isOpen, title, ticketsUpdated, onClose } = this.props;

    if (!isOpen) {
      return null;
    }

    return (
      <Modal
        onClose = {onClose}
        heading = { title }
        width = {'large'}>
        <ContentWrapper>
          {
            ticketsUpdated.map((ticket, idx) =>
              <UpdateWrapper key={idx}>
                <TicketUpdate {...ticket}/>
              </UpdateWrapper>
            )
          }
        </ContentWrapper>
      </Modal>
    );
  }
}

export default TicketListModal;

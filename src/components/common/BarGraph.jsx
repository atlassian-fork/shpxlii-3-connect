import _ from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';
import Tooltip from '@atlaskit/tooltip';

import TicketListModal from '../common/TicketListModal.jsx';
import TicketUpdate from '../common/TicketUpdate.jsx';

const DEFAULT_STYLE = {
  borderRadius:6,
  fontSize: 14,
};

const LARGE_STYLE = {
  borderRadius: 12,
  fontSize: 24,
};

const getStyleForSize = (size) => {
  switch (size) {
    case 'large':
      return LARGE_STYLE;
    default:
      return DEFAULT_STYLE;
  }
};

const getBorderRadius = ({ isFirst, isLast, borderRadius }) => {
  if (isFirst && isLast) {
    return `${borderRadius}px`;
  }

  if (isFirst) {
    return `${borderRadius}px 0 0 ${borderRadius}px`;
  }

  if (isLast) {
    return `0 ${borderRadius}px ${borderRadius}px 0`;
  }

  return '0';
};

const BarItem = styled.a`
  border-radius: ${getBorderRadius};
  background-color: ${(props) => props.color};
  color: #FFFFFF;
  font-size: ${(props) => props.fontSize}px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
  box-sizing: border-box;
  
  &:hover {
    border-color: #000000;
    border-style: solid;
    border-width: 2px;
    color: #FFFFFF;
    text-decoration: none;
  }
`;

const BarWrapper = styled.div`
  display: flex;
  flex-direction: row;
  height: 100%;
`;

const WrappedTooltip = styled(Tooltip)`
  width: ${(props) => props.percentage}%;
  height: 100%;
`;

class BarGraph extends React.PureComponent {
    static propTypes = {
      size: PropTypes.string,
      cutoff: PropTypes.number,
      data: PropTypes.arrayOf(
        PropTypes.shape({
          collectionName: PropTypes.string.isRequired,
          color: PropTypes.string.isRequired,
          count: PropTypes.number.isRequired,
          name: PropTypes.string.isRequired,
          ticketsUpdated: PropTypes.arrayOf(PropTypes.shape(TicketUpdate.propTypes)).isRequired,
        })
      ).isRequired
    }

  state = { isModalOpen: false, selectedItem: null }

  closeModal = () => this.setState({ isModalOpen:false, selectedItem: null });

  onClickBarItem = (item) => {
    this.setState({
      selectedItem: item,
      isModalOpen: true,
    });
  }

  renderModal() {
    const { isModalOpen, selectedItem } = this.state;
    if (!isModalOpen) {
      return null;
    }

    const { name, ticketsUpdated, collectionName } = selectedItem;

    const title = `Tickets updated to ${name} in ${collectionName}`;
    return <TicketListModal title={title}
      ticketsUpdated={ticketsUpdated}
      isOpen={isModalOpen}
      onClose={this.closeModal}
    />;
  }

  render() {
    const { data, size, cutoff } = this.props;

    const totalCount = data.reduce((sum, item) => sum + item.count, 0);

    const children = _(data)
      .sortBy('count')
      .map((item, index) => {
        const { count, color, name } = item;
        const percentage = 100 * count / totalCount;
        const style = getStyleForSize(size);
        const tooltip = `${name} (${count}/${totalCount})`;

        return (
          <WrappedTooltip content={tooltip} percentage={percentage} key={index}>
            <BarItem
              color={color}
              isFirst={index === 0}
              isLast={index === data.length - 1}
              {...style}
              onClick={() => this.onClickBarItem(item)}
            >
              {percentage > cutoff ? count: ''}
            </BarItem>
          </WrappedTooltip>
        );
      })
      .value();
    return (
      <BarWrapper>
        {children}
        {this.renderModal()}
      </BarWrapper>
    );
  }
}

export default BarGraph;

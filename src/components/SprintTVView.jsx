import React from 'react';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import styled from 'styled-components';
import '@atlaskit/css-reset';

import { getData } from '../data/sprint';
import { getQueryParameter } from '../utils';

import SprintHeader from './header/SprintHeader.jsx';
import MainContent from './main-content/MainContent.jsx';
import Sidebar from './sidebar/Sidebar.jsx';
import Loading from './common/Loading.jsx';

const Wrapper = styled.div`
  padding: 0 16px;
`;

export default class SprintTVView extends React.PureComponent {
  constructor() {
    super();
    this.state = {};
  }

  componentDidMount() {
    getData(getQueryParameter('boardId')).then((board) => {
      this.setState({
        ...board,
        done: true,
      });
    });
  }

  render() {
    if (!this.state.done) {
      return <Loading/>;
    }

    const { headerProps, mainContentProps, sidebarProps } = this.state;

    return (
      <Wrapper>
        <Page>
          <Grid layout="fluid">
            <GridColumn medium={12}>
              <SprintHeader {...headerProps} />
            </GridColumn>
            {
              (headerProps.epicCount > 0 || headerProps.issueCount > 0) ?
                [
                  <GridColumn medium={8}>
                    <MainContent {...mainContentProps} />
                  </GridColumn>,
                  <GridColumn medium={4}>
                    <Sidebar {...sidebarProps} />
                  </GridColumn>
                ]
                : null
            }
          </Grid>
        </Page>
      </Wrapper>
    );
  }
}

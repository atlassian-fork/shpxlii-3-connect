FROM node:8.9.4-slim

run mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app

EXPOSE 8080

CMD ["npm", "start"]

#!/usr/bin/env bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PARENT_DIR="$(dirname "${SCRIPT_DIR}")"
. ${SCRIPT_DIR}/env.sh

npm install
npm run build

# Build Docker containers
docker-compose -f ${PARENT_DIR}/docker-compose.yml build

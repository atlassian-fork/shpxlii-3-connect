#!/usr/bin/env bash

set -e

ENV=${1:-"ddev"}
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PARENT_DIR="$(dirname "${SCRIPT_DIR}")"
. ${SCRIPT_DIR}/env.sh
. ${SCRIPT_DIR}/build.sh

echo "Deploying to ${ENV}"
echo "Installing Micros CLI..."
npm install -g @atlassian/micros-cli

docker push ${DOCKER_IMAGE}:${DOCKER_TAG}
echo "=== Docker container pushed."

# For Bamboo
MICROS_TOKEN="${bamboo_MICROS_TOKEN_PASSWORD}"
micros service:deploy ${SERVICE_NAME} -f ${PARENT_DIR}/jira-boards-on-tv.sd.yml -u cfn-base -e $ENV
echo "=== Service deployed."
